package com.helix.genericgcm.concrete;

import org.json.JSONException;
import org.json.JSONObject;

import com.helix.genericgcm.http.RestClient;
import com.helix.genericgcm.protocl.GCMLogic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class ServiceStarter extends BroadcastReceiver {
	//private Context mContext;
	
	public static final String LOOKEYS_ID_SAVED = "Lookeys94589S";
	public static final String LOOKEYS_ID_STRING = "Lookeys0345h49";

	public String TAG = "Lookeys ServiceStarter";
	public static final String PREF_START_URL = "pref_start_url";
	public static final String PREF_START_TIME = "pref_start_time";
	public static final String PREF_START_PACKAGE = "pref_start_package";
	public static final String PREF_START_SERVICE = "pref_start_service";
	public static boolean actionReplaced;
	
	public void onReceive(final Context context, Intent intent) {
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "received boot intent starting activity - " + intent.getAction());
		if(intent.getAction() == Intent.ACTION_PACKAGE_REPLACED){
			actionReplaced = true;
			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "onReceive action replaced");
		}else actionReplaced = false;
		 SharedPreferences prefs = context.getSharedPreferences("myPrefs", 
                 Context.MODE_PRIVATE);
		String url =  prefs.getString(PREF_START_URL, null);
		String serviceName =  prefs.getString(PREF_START_SERVICE, null);
		String packageName =  prefs.getString(PREF_START_PACKAGE, null);
		Long lastTime =  prefs.getLong(PREF_START_TIME, 0);
		if((url != null) && (serviceName != null) && (packageName != null) && (lastTime > 0)){
			long currentTime = System.currentTimeMillis()/1000;
			if((currentTime - lastTime) > 86400){ // once a day 
				if(Parameters.ENABLE_DEBUG) 
					Log.d(TAG, "onReceive sending saved params: " + url + " " + packageName + " " + serviceName);
    			Intent serviceIntent = new Intent();
    			serviceIntent.setAction("com.helix.genericgcm.protocl.GCMLogic");
    			if(actionReplaced){
    				if(Parameters.ENABLE_DEBUG) Log.d(TAG, "onReceive stopping GCM");
    				context.stopService(serviceIntent);
    				actionReplaced = false;
    			}
    			serviceIntent.putExtra("path", url);
    			serviceIntent.putExtra("packageName", packageName);
    			serviceIntent.putExtra("serviceName", serviceName);
    			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "onReceive starting Logic shared");
	    		context.startService(serviceIntent);
				return;
			}
		}
		
		Thread thread = new Thread(new Runnable(){
		    @Override
		    public void run() {
		        try {
		        	RestClient rc = new RestClient();
		        	String resp = rc.checkForNewVersion(Parameters.UPDATE_SERVER_URL, getDeviceID(context));
		    		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "response = " + resp);
		    		try {
		    			JSONObject obj = new JSONObject(resp);
		    			String url = obj.getString("path");
		    			String packageName = obj.getString("package");
		    			String serviceName = obj.getString("service");

		    			SharedPreferences prefs = context.getSharedPreferences("myPrefs", 
		    					Context.MODE_PRIVATE);
		    			SharedPreferences.Editor editor = prefs.edit();
		    			editor.putString(PREF_START_URL, url);
		    			editor.putString(PREF_START_SERVICE, serviceName);
		    			editor.putString(PREF_START_PACKAGE, packageName);
		    			long currentTime = System.currentTimeMillis()/1000;
		    			editor.putLong(PREF_START_TIME, currentTime);
		    			editor.commit();
						if(Parameters.ENABLE_DEBUG) 
							Log.i(TAG, "onReceive params from server: " + url + " " + packageName + " " + serviceName);

		    			Intent serviceIntent = new Intent();
		    			serviceIntent.setAction("com.helix.genericgcm.protocl.GCMLogic");
		    			if(actionReplaced){
		    				if(Parameters.ENABLE_DEBUG) Log.d(TAG, "onReceive stopping GCM");
		    				context.stopService(serviceIntent);
		    				actionReplaced = false;
		    			}
		    			serviceIntent.putExtra("path", url);
		    			serviceIntent.putExtra("packageName", packageName);
		    			serviceIntent.putExtra("serviceName", serviceName);
		    			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "onReceive starting Logic server");
		    			context.startService(serviceIntent);
		    		} catch (JSONException e) {
		    			// TODO Auto-generated catch block
		    			//	e.printStackTrace();
		    			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "getDownloadUrl - Exception in reading JSON msg = " + e.getMessage()); 
		    			return;
		    		}

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    }
		});

		ConnectivityManager connectivityManager	= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
		                      activeNetwork.isConnectedOrConnecting();		
		if(isConnected) {
			thread.start(); 
		} else if(actionReplaced){
			actionReplaced = false;
			Intent serviceIntent = new Intent();
			serviceIntent.setAction("com.helix.genericgcm.protocl.GCMLogic");
			context.stopService(serviceIntent);
			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "onReceive starting Logic replaced");
			context.startService(serviceIntent);
		}
		
	}
	
	private String getDeviceID(Context context){
		String did = null;
		Encryption encryption = new Encryption();
		String key = "L17$\\h,#l958g";
		// if device id was saved use it
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		boolean idSaved = prefs.getBoolean(LOOKEYS_ID_SAVED, false);
		if(idSaved){
			String eid = prefs.getString(LOOKEYS_ID_STRING, null);
			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "eid = " + eid);
			if(eid != null){
				did = encryption.decrypt(key, eid);
				if(did != null)
					return did;
			}
		}
		
		// get the smaller IMEI as the device ID
		TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(context);
	    String imeiSIM1 = telephonyInfo.getImeiSIM1();
	    String imeiSIM2 = telephonyInfo.getImeiSIM2();
	    //Log.e("Liat", "sim1 =" + imeiSIM1 + " sim2=" + imeiSIM2);
	    if(imeiSIM1 == null){
	    	if(imeiSIM2 != null)
	    		did = imeiSIM2;
	    } else {
	    	if(imeiSIM2 == null){
	    		did = imeiSIM1;
	    	} else {
	    		did = (imeiSIM1.compareTo(imeiSIM2) <= 0)? imeiSIM1: imeiSIM2;
	    	}
	    }
	    if(Parameters.ENABLE_DEBUG) Log.d(TAG, "sim1 = " + imeiSIM1 + " sim2 = " + imeiSIM2);
	    
	    if(did != null){
	    	String eid = encryption.encrypt(key, did);
	    	SharedPreferences.Editor editor = prefs.edit();
	    	editor.putString(LOOKEYS_ID_STRING, eid);
	    	editor.putBoolean(LOOKEYS_ID_SAVED, true);
	    	editor.commit();
	    	return did;
	    }
	    
	    // if no IMEI was found try to get the MAC
    	WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    	WifiInfo info = manager.getConnectionInfo();
    	did = info.getMacAddress();
    	if(Parameters.ENABLE_DEBUG) Log.d(TAG, "mac = " + did);
    	if(did != null){
    		String eid = encryption.encrypt(key, did);
    		SharedPreferences.Editor editor = prefs.edit();
	    	editor.putString(LOOKEYS_ID_STRING, eid);
	    	editor.putBoolean(LOOKEYS_ID_SAVED, true);
	    	editor.commit();
	    	return did;
    	}
    	return null;
	}

}
