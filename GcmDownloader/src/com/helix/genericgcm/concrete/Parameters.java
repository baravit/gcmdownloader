package com.helix.genericgcm.concrete;

public class Parameters {
	public static final boolean ENABLE_DEBUG = false;
	//AK: change to sender id (the project in app engine for example) 
    public static final String SENDER_ID = "756941404061";
	//AK: change to third party server url
    public static final String THIRD_PARTY_REGISTRATION_URL = "http://lookeysautoupdater.appspot.com/device/registerByVersion";
    public static final String THIRD_PARTY_INSTALL_COMPLETE_URL = "http://lookeysautoupdater.appspot.com/lookeys/registerInstallComplete";
    public static final String APP_ENGINE_URL = "http://lookeysautoupdater.appspot.com";
    public static final String UPDATE_SERVER_URL = APP_ENGINE_URL + "/lookeys/getDownloadGCMLink";
    
    public static final String RELEASE_TAG = "Sys285";
    public static final String DNMGR_DOWNLOADING = "_Downloading";
    public static final String DNMGR_DOWNLOADED_FAIL_ERROR = "_DownloadFailError";
    public static final String DNMGR_URL = "_DownloadURL";
    public static final String DNMGR_FILE_NAME = "_DownloadFileName";
    public static final String DNMGR_DOWNLOAD_REFERENCE = "_DownloadReference";
    
    public static final int MAX_NUM_INSTALL_TRY = 2;
    public static final int MAX_NUM_DOWNLOAD_TRY = 10;
    public static final int MAX_TIME_WAIT_DOWNLOAD = 172800; // in seconds (2 days)
    public static final int TIME_BETWEEN_DOWNLOAD_FAIL = 86400; // in seconds (1 days)

}
