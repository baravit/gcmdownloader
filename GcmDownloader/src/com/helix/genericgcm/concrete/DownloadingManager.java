// This class will write the SharedPreference the following with a sharedPrefsPrefix prefix:
// _Downloading (int)
//					0 - not downloading
//					1 - downloading
//					2 - completed success
//					3 - completed fail
// _DownloadFailError (int)
// _DownloadURL (string)
// _DownloadFileName (string)
// _DownloadReference (long)

package com.helix.genericgcm.concrete;

import java.io.File;

import com.helix.genericgcm.protocl.GCMLogic;
import com.helix.genericgcm.protocl.GCMLogic.DownloadCallbackInterface;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;


public class DownloadingManager {
	

	private static final String TAG = "Lookeys DownloadingManager";
	private DownloadManager downloadManager;
	private long downloadReference;
	private DownloadCallbackInterface downloadCompleteCallback;
	
	// sharedPrefsPrefix - the prefix to add to Shered Preference Keys (default = "")
	// shouldInstall - true to install the downloaded apk
	public DownloadingManager(Context context, DownloadCallbackInterface callback){
		downloadCompleteCallback = callback;
	}
	
	public static String extractDownloadPrefix(String url){
		int ind = url.lastIndexOf('/');
		if(ind <= 0)
			return null;
		String prefix = url.substring(ind+1);
		if(prefix.length() == 0)
			return null;
		return prefix;
	}

	// UseExternal = true - will download the file to ExternalStorageDirectory
	// UseExternal = false - will download the file to /data/data/<package name>/files
	public long downloadFile(Context context, String url, boolean UseExternal){
		String spPrefix = extractDownloadPrefix(url);
		if(spPrefix == null){
			if(Parameters.ENABLE_DEBUG)
				Log.e(TAG, "downloadFile Error! prefix is null - url!!!");
			return -1;
		}
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(spPrefix + Parameters.DNMGR_DOWNLOADING, 0);
		editor.putInt(spPrefix + Parameters.DNMGR_DOWNLOADED_FAIL_ERROR, 0);
		editor.putString(spPrefix + Parameters.DNMGR_URL, "");
		editor.putString(spPrefix + Parameters.DNMGR_FILE_NAME, "");
		editor.putLong(spPrefix + Parameters.DNMGR_DOWNLOAD_REFERENCE, 0);
		editor.commit();

		downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		context.registerReceiver(downloadReceiver, filter);

		editor.putInt(spPrefix + Parameters.DNMGR_DOWNLOADING, 1);
		editor.putString(spPrefix + Parameters.DNMGR_URL, url);
		
		Uri Download_Uri = Uri.parse(url);
		DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

		//Restrict the types of networks over which this download may proceed.
		//request.setAllowedNetworkTypes(Request.NETWORK_WIFI);
		//Set whether this download may proceed over a roaming connection.
		request.setAllowedOverRoaming(false);
		//Set the title of this download, to be displayed in notifications (if enabled).
		request.setTitle("My Data Download");
		//Set a description of this download, to be displayed in notifications (if enabled)
		request.setDescription("Android Data download using DownloadManager.");
		// we just want to download silently
		request.setVisibleInDownloadsUi(false); 
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);

		boolean externalFailed = false;
		if(UseExternal){
			//Set the local destination for the downloaded file to a path within the application's external files directory
			try{
				request.setDestinationInExternalFilesDir(context,Environment.getExternalStorageDirectory().getAbsolutePath(),"up.apk");
			} catch (Exception e){
				externalFailed = true;
				if(Parameters.ENABLE_DEBUG)
					Log.e(TAG, "downloadFile exception in downloading to external message = " + e.getMessage());
			}
		} else externalFailed = true;
		if(externalFailed){
			//Set the local destination for the downloaded file to a path within the application's files directory
			try{
				request.setDestinationInExternalFilesDir(context,context.getFilesDir().getAbsolutePath(),"up.apk");
			} catch (Exception e){
				if(Parameters.ENABLE_DEBUG)
					Log.e(TAG, "downloadFile exception in downloading to INTERNAL message = " + e.getMessage());
			}
		}
		
		//Enqueue a new download and same the referenceId
		downloadReference = downloadManager.enqueue(request);
		editor.putLong(spPrefix + Parameters.DNMGR_DOWNLOAD_REFERENCE, downloadReference);
		editor.commit();

		return downloadReference;
	}

	// cancel the download
	// make sure this is the correct downloadRef
	public static void cancelDownload(Context context, long downloadRef, String url){
		((DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE)).remove(downloadRef);
	}
	
	// check the status of the download
	// checks that downloadRef matches url
	// returns:
	//    	-2 - downloadRef does not match url
	//		-1 - failed
	//		 0 - succedded
	//       1 - waiting
	public static int isDownloading(Context context, long downloadRef, String url){
		DownloadManager mngr = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		DownloadManager.Query query = new DownloadManager.Query();
		query.setFilterById(downloadRef);
		Cursor cursor = mngr.query(query);
		
		if (!cursor.moveToFirst()) {
			if(Parameters.ENABLE_DEBUG)
				Log.e(TAG, "isDownloading Error! cursor is empty - Should not get here!!!");
		    return -1;
		}
		
		int uriIndex = cursor.getColumnIndex(DownloadManager.COLUMN_URI);
		String urlString = cursor.getString(uriIndex);
		if(Parameters.ENABLE_DEBUG)
			Log.d(TAG,"isDownloading url = " + url + " uri = " + urlString);
		if(!url.equals(urlString)){
			if(Parameters.ENABLE_DEBUG)
				Log.e(TAG,"isDownloading downloadRef does not match url");
			return -2;
		}
		int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
		int status = cursor.getInt(statusIndex);
		
		if(status == DownloadManager.STATUS_SUCCESSFUL)
			return 0;
		if(status == DownloadManager.STATUS_FAILED)
			return -1;
		return 1;
	}
	
	public static String getDownloadedFile(Context context, long ref){
		DownloadManager mngr = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		DownloadManager.Query query = new DownloadManager.Query();
		query.setFilterById(ref);
		Cursor cursor = mngr.query(query);
		
		if (!cursor.moveToFirst()) {
			if(Parameters.ENABLE_DEBUG)
				Log.e(TAG, "isDownloading Error! cursor is empty - Should not get here!!!");
		    return null;
		}
		int filenameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
		return cursor.getString(filenameIndex);
	}

	private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			//check if the broadcast message is for our Enqueued download
			long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "onReceive downloadReference = " + downloadReference + " referenceId = " + referenceId);
			if(downloadReference == referenceId){

				DownloadManager.Query query = new DownloadManager.Query();
				query.setFilterById(referenceId);
				Cursor cursor = downloadManager.query(query);
				if (!cursor.moveToFirst()) {
					if(Parameters.ENABLE_DEBUG)
						Log.e(TAG, "onReceive Error! cursor is empty - Should not get here!!!");
				    return;
				}
				
				int urlIndex = cursor.getColumnIndex(DownloadManager.COLUMN_URI);
				String url = cursor.getString(urlIndex);
				String prefix = extractDownloadPrefix(url);
				if(prefix == null){
					if(Parameters.ENABLE_DEBUG)
						Log.e(TAG, "downloadFile Error! prefix is null - url!!!");
					return;
				}

				if(Parameters.ENABLE_DEBUG)
					Log.d(TAG, "onReceive status = " + GetStatusString(cursor));
				int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
				int status = cursor.getInt(statusIndex);
				
				SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
		                Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				editor.commit();

				
				// handle fail
				if (DownloadManager.STATUS_SUCCESSFUL != status) {
					if(Parameters.ENABLE_DEBUG)
						Log.w(TAG, "onReceive Download Failed");
					// handle only fail - other status is pending or waiting which do not need handling
					if(status == DownloadManager.STATUS_FAILED){
						editor.putInt(prefix + Parameters.DNMGR_DOWNLOADING, 3);
						int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
						int reason = cursor.getInt(columnReason);
						editor.putInt(prefix + Parameters.DNMGR_DOWNLOADED_FAIL_ERROR, reason);
					}
					editor.commit();
					downloadCompleteCallback.onDownloadFinished(false);
				    return;
				}

				editor.putInt(prefix + Parameters.DNMGR_DOWNLOADING, 2);
				//get the download filename
				int filenameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
				String filename = cursor.getString(filenameIndex);
				
				int uriIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
				String downloadedPackageUriString = cursor.getString(uriIndex);
				
				int sizeIndex = cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
				int size = cursor.getInt(sizeIndex);
				
				int downSizeIndex = cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
				int downSize = cursor.getInt(downSizeIndex);
				
				File f = new File(filename);
				long fileSize = 0;
				if(f.exists()){
					fileSize = f.length();
				}
				
				if(Parameters.ENABLE_DEBUG){
					Log.i(TAG, "onReceive download complete ... " + downloadedPackageUriString );
					Log.i(TAG, "onReceive downloaded fileName =  " + filename );
					Log.i(TAG, "onReceive total = " + size + " downloaded = " + downSize + " file size = " + fileSize);
				}
				editor.putString(prefix + Parameters.DNMGR_FILE_NAME, filename);
	    		editor.commit();

				downloadCompleteCallback.onDownloadFinished(true);
			}
		}
	}; 
	

	private String GetStatusString(Cursor cursor){

		//column for status
		int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
		int status = cursor.getInt(columnIndex);
		//column for reason code if the download failed or paused
		int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
		int reason = cursor.getInt(columnReason);
		//get the download filename
		int filenameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
		String filename = cursor.getString(filenameIndex);

		String statusText = "";

		switch(status){
		case DownloadManager.STATUS_FAILED:
			statusText = "STATUS_FAILED - ";
			switch(reason){
			case DownloadManager.ERROR_CANNOT_RESUME:
				statusText += "ERROR_CANNOT_RESUME";
				break;
			case DownloadManager.ERROR_DEVICE_NOT_FOUND:
				statusText += "ERROR_DEVICE_NOT_FOUND";
				break;
			case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
				statusText += "ERROR_FILE_ALREADY_EXISTS";
				break;
			case DownloadManager.ERROR_FILE_ERROR:
				statusText += "ERROR_FILE_ERROR";
				break;
			case DownloadManager.ERROR_HTTP_DATA_ERROR:
				statusText += "ERROR_HTTP_DATA_ERROR";
				break;
			case DownloadManager.ERROR_INSUFFICIENT_SPACE:
				statusText += "ERROR_INSUFFICIENT_SPACE";
				break;
			case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
				statusText += "ERROR_TOO_MANY_REDIRECTS";
				break;
			case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
				statusText += "ERROR_UNHANDLED_HTTP_CODE";
				break;
			case DownloadManager.ERROR_UNKNOWN:
				statusText += "ERROR_UNKNOWN";
				break;
			}
			break;
		case DownloadManager.STATUS_PAUSED:
			statusText = "STATUS_PAUSED - ";
			switch(reason){
			case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
				statusText += "PAUSED_QUEUED_FOR_WIFI";
				break;
			case DownloadManager.PAUSED_UNKNOWN:
				statusText += "PAUSED_UNKNOWN";
				break;
			case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
				statusText += "PAUSED_WAITING_FOR_NETWORK";
				break;
			case DownloadManager.PAUSED_WAITING_TO_RETRY:
				statusText += "PAUSED_WAITING_TO_RETRY";
				break;
			}
			break;
		case DownloadManager.STATUS_PENDING:
			statusText = "STATUS_PENDING";
			break;
		case DownloadManager.STATUS_RUNNING:
			statusText = "STATUS_RUNNING";
			break;
		case DownloadManager.STATUS_SUCCESSFUL:
			statusText = "STATUS_SUCCESSFUL - ";
			statusText += "Filename:\n" + filename;
			break;
		}
		return statusText;
	}
}

