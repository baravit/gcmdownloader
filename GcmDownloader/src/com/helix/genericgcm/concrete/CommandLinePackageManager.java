package com.helix.genericgcm.concrete;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import android.util.Log;

public class CommandLinePackageManager {
	private static final String TAG = "LookeysGCM CommandLinePackageManager";
	public String mCommand;
	
	public boolean runCommandLine(String commandLine){
		mCommand = commandLine;
		try {
			
			Process process = Runtime.getRuntime().exec(commandLine);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String line = null;
			do{
				line = bufferedReader.readLine();
				if(null != line){
					if(Parameters.ENABLE_DEBUG) Log.i(TAG, line);
					if(line.contains("Failure")){
						return false;
					}
				}
			}
			while(null != line);
			return true;
		} catch (Exception e) {
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "command line exception: " + e.getMessage());
			return false;
		}
	}
	
	
	public void upgradeApkByCommandLine(String path){
		String commandLine = "pm install -r " + path;

		if(!runCommandLine(commandLine)){
			commandLine = "pm install -r -s " + path;
			runCommandLine(commandLine);
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "failed to install apk: " + path);
		}else{
			if(Parameters.ENABLE_DEBUG)
				Log.i(TAG, "installed aplication " + path);
			else Log.i(Parameters.RELEASE_TAG, "Sys Msg 20");
		}
	}
	
}
